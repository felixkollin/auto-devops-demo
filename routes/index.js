var express = require('express')
var router = express.Router()
var storage = require('../storage.js')
const axios = require('axios')

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('index', { title: 'Express' })
})

router.get('/retry-pipeline', async (req, res) => {
  pipeline_id = req.query.pipeline_id

  const token = await storage.getItem('token')

  axios.post('https://gitlab.com/api/v4/projects/12162922/pipelines/' +  pipeline_id + '/retry', {}, {

    headers: { 'Authorization': 'Bearer ' + token }
  })
  .then((gitLabRes) => {
    res.json(gitLabRes.data)
  })
  .catch((error) => {
    console.error(error)
  })

})

router.get('/pipeline-jobs', async (req, res) => {
  pipeline_id = req.query.pipeline_id

  const token = await storage.getItem('token')

  axios.get('https://gitlab.com/api/v4/projects/12162922/pipelines/' + pipeline_id + '/jobs', {
    headers: { 'Authorization': 'Bearer ' + token }
  })
  .then((gitLabRes) => {
    res.json(gitLabRes.data)
  })
  .catch((error) => {
    console.error(error)
  })
})

router.get('/pipelines', async (req, res) => {
  const token = await storage.getItem('token')

  axios.get('https://gitlab.com/api/v4/projects/12162922/pipelines', {
    headers: { 'Authorization': 'Bearer ' + token }
  })
    .then((gitlabRes) => {
      res.json(gitlabRes.data)
    })
    .catch((error) => {
      console.error(error)
    })
})

router.get('/demo', (req, res, next) => {
  res.render('demo')
})

router.post('/login', function (req, res, next) {
  const { username, password } = req.body
  storage.storeItem('username', username)
  storage.storeItem('password', password)

  axios.post('https://gitlab.com/oauth/token', {
    grant_type: 'password',
    password,
    username
  })
    .then((gitlabRes) => {
      storage.storeItem('token', gitlabRes.data.access_token)
      res.redirect('/demo')
    })
    .catch((error) => {
      console.error(error)
    })
})

module.exports = router
