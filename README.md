## Pipeline Dashboard
This project visualizes runs of Gitlab pipelines with circles.
The largest circle is the most recent run, the smallest is the most old one. The placement of circles is randomized.

### The purpose
The dashboard gives you a quick overview of the health of your pipelines.
It looks beautiful on a large monitor and is an excellent choice to show on the walls of your office!

### Easter egg
If a pipeline fails the dashboard screen will flash in red and play an alarm sound so that everyone is made aware of the failure and can quickly get to work and fix it.

![Imgur](https://i.imgur.com/DdRfMzS.png, "Screenshot of the Pipeline Dashboard")