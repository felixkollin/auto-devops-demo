const store = require('node-persist');

(async () => {
  await store.init( /* options ... */ );
})();

const storeItem = async (key, val) => {
  await store.setItem(key, val);
}

const getItem = key => {
  return store.getItem(key)
}

module.exports = { storeItem, getItem };